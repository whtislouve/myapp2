/**
 * @format
 */

import {AppRegistry} from 'react-native';
import { Authorisation } from './modules/authorisation/view/Authorisation';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Authorisation);
