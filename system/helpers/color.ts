export enum Color {
    white = '#FFFFFF',
    authorisationWord = '#2f2f2f',
    words = '#2f2f2f',
    login = '#d0d0d0',
    forgotPassword = '#2cb76c',
    enterButton = '#7a4d9f',
    backColorSwitch = '#73b397',
    backColorPasswordModal = '#00c46e',
    }
