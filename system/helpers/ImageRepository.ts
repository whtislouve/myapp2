export class ImageRepository {
    static readonly logo = require("../../assets/images/logo.png")
    static readonly questionMark = require("../../assets/images/questionMark.png")
    static readonly dottedLine = require("../../assets/images/dottedLine.png")
    static readonly underLine = require("../../assets/images/underLine.png")
    static readonly questionMarkModal = require("../../assets/images/questionMarkModal.png")
    static readonly closeModal = require("../../assets/images/closeModal.png")
    static readonly lineModal = require("../../assets/images/lineModal.png")

}
