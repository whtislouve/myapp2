import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  Text,
  TouchableOpacityProps,
  TextStyle,
  View,
} from 'react-native'

interface IState {

}

interface IProps extends TouchableOpacityProps{
  title: string
  textStyle: TextStyle
}

export class CustomButton extends PureComponent<IState, IProps> {
  render(){
    const {
      title,
      textStyle,
    } = this.props
    return(
      <View>
        <TouchableOpacity
          {...this.props}
        >
          <View >
            <Text style={textStyle}>
              {title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
