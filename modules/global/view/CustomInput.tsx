import React, { PureComponent } from 'react'
import {
  View,
  TextInput,
  TextInputProps,
  TextStyle,
  StyleSheet,
  Text,
} from 'react-native'
import { style } from '../../../system/helpers/style'
import {windowWidth} from '../../../system/helpers/dimension'
import {Color} from '../../../system/helpers/color'
import {Fonts} from '../../../system/helpers/fonts'

interface IState{

}

interface IProps extends TextInputProps{
  text: string
  ViewStyle?: TextStyle
  inputStyle?: TextStyle
  textStyle?: TextStyle
}

export class CustomInput extends PureComponent<IState, IProps>{
  render(){
    const{
      text,
      ViewStyle,
      inputStyle,
      textStyle,
    } = this.props

    const inputFlattenStyle = StyleSheet.flatten([
      inputStyle,
      styles.loginPasswordInput,
    ])

    const titleStyleFlatten = StyleSheet.flatten([
      styles.text,
      styles.loginPasswordTitle,
      textStyle,
    ])

    return(
      <View>
        <TextInput
          style={inputFlattenStyle}
        />
        <Text style={titleStyleFlatten}>
          {text}
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loginPasswordTitle: style.text({
    position: 'absolute',
    left: windowWidth * 0.1,
    top: windowWidth * 0.02,
    backgroundColor: Color.white,
    paddingHorizontal: windowWidth * 0.02,
  }),
  loginPasswordInput: style.text({
    width: windowWidth * 0.97,
    height: windowWidth * 0.13,
    borderRadius: windowWidth * 0.065,
    borderWidth: windowWidth * 0.0025,
    borderColor: Color.login,
  }),
  text: style.text({
    left: windowWidth * 0.1,
    top: windowWidth * 0.02,
  }),
})

