import React, { PureComponent } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native'
import { ImageRepository } from '../../../system/helpers/ImageRepository'
import { windowWidth } from '../../../system/helpers/dimension'
import { style } from '../../../system/helpers/style'
import { Color } from '../../../system/helpers/color'
import { CustomInput } from '../../global/view/CustomInput'
import { CustomButton } from '../../global/view/CustomButton'
import { Switch } from 'react-native-switch'
import { Fonts } from '../../../system/helpers/fonts'
import Modal from 'react-native-modal'

interface IState {
  switchValue: boolean
  switchModal: boolean
}

interface IProps {

}

export class Authorisation extends PureComponent<IState, IProps> {
  state = {
    switchValue: false,
    switchModal: false,
  }

  toggleSwitch = () => {
    this.setState({ switchValue: !this.state.switchValue })
  }

  toggleModal = () => {
    this.setState({ switchModal: !this.state.switchModal })
  }

  onChange = () => {
    Alert.alert("АСУ")
  }
  render(){
    return(
      <View style={styles.container}>
        <Image
          source={ ImageRepository.logo }
          style={styles.logo}
        />
        <Text style={styles.authorisationWord}>
          Авторизация
        </Text>
        <Text style={styles.words}>
          Введите логин и пароль, {"\n "}полученные при регистрации
        </Text>

          <CustomInput
            onChange={this.onChange}
            inputStyle={styles.loginPasswordInput}
            titleStyleFlatten={styles.loginPasswordTitle}
            text='Логин'
          />
          <CustomInput
            onChange={this.onChange}
            inputStyle={styles.loginPasswordInput}
            titleStyleFlatten={styles.loginPasswordTitle}
            text='Пароль'
          />

        <View style={styles.containerAccess}>
          <Text style={styles.accessCode}>
            Есть код доступа?
          </Text>
          <TouchableOpacity onPress={this.onChange}>
            <Image
              style={styles.questionMark}
              source={ ImageRepository.questionMark }/>
          </TouchableOpacity>
          <Text style={styles.forgotPassword}>
            Забыл пароль?
          </Text>
        </View>
        <View style={styles.dottedLineContainer}>
          <Image
            style={styles.dottedLine}
            source={ ImageRepository.dottedLine }/>
        </View>
        <View style={styles.containerButtons}>
          <CustomButton
            title='Регистрация'
            style={styles.registrationButton}
            textStyle={styles.textReg}
          />
          <CustomButton
            onPress={this.toggleModal}
            value={this.state.switchModal}
            title='Войти'
            style={styles.enterButton}
            textStyle={styles.textEnter}
          />
        </View>
        <View style={styles.politTextContainer}>
          <View style={styles.styleSwitchContainer}>
            <Switch
              onValueChange={this.toggleSwitch}
              value={this.state.switchValue}
              activeText={''}
              inActiveText={''}
              barHeight={windowWidth * 0.049}
              circleSize={windowWidth * 0.069}
              backgroundActive={Color.backColorSwitch}
              circleActiveColor={Color.forgotPassword}
            />
          </View>
          <View>
            <Text style={styles.politText}>
              Согласен на обработку персональных данных {"\n"}
              и принимаю условия Политики конфиденциальности.
            </Text>
            <Image
              style={styles.underLine}
              source={ ImageRepository.underLine }/>
          </View>
        </View>
        <Modal isVisible={this.state.switchModal}>
          <View style={styles.modalContent}>
            <View style={styles.headerModal}>
              <TouchableOpacity onPress={this.onChange}>
                <Image
                  style={styles.questionMarkModal}
                  source={ ImageRepository.questionMarkModal }/>
              </TouchableOpacity>
              <Text style={styles.headerTextModal}>
                Неверный пароль
              </Text>
              <TouchableOpacity onPress={this.toggleModal}
              >
                <Image
                  style={styles.closeModal}
                  source={ ImageRepository.closeModal }/>
              </TouchableOpacity>
            </View>
            <View style={styles.lineModalView}>
              <Image
                style={styles.lineModal}
                source={ ImageRepository.lineModal }/>
            </View>
            <Text style={styles.textModal}>
              Вы ввели неверный пароль,{"\n"}
              попробуйте снова
            </Text>
            <View style={styles.passwordModal}>
              <CustomButton
                title='Ввести пароль'
                textStyle={styles.textPasswordModal}
              />
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: Color.white
  },
  logo: style.image({
    width: windowWidth * 0.93,
    height: windowWidth * 0.21,
    marginTop: windowWidth * 0.1,
    marginLeft: windowWidth * 0.067,
    marginRight: windowWidth * 0.067,
  }),
  authorisationWord: style.text({
    width: windowWidth * 0.65,
    height: windowWidth * 0.13,
    marginTop: windowWidth * 0.06,
    marginLeft: windowWidth * 0.21,
    marginRight: windowWidth * 0.21,
    color: Color.authorisationWord,
    fontSize: windowWidth * 0.096,
    fontFamily: Fonts.geometriaBold,

  }),
  words: style.text({
    width: windowWidth * 0.73,
    height: windowWidth * 0.13,
    marginTop: windowWidth * 0.046,
    marginLeft: windowWidth * 0.17,
    marginRight: windowWidth * 0.16,
    color: Color.words,
    textAlign: 'center',
    fontSize: windowWidth * 0.048,
    fontFamily: Fonts.geometria,
  }),
  customInputContainer: style.view({
    flexDirection: 'column',
    justifyContent: 'space-between',
  }),
  loginPasswordInput: style.text({
    marginTop: windowWidth * 0.046,
    marginLeft: windowWidth * 0.04,
    marginRight: windowWidth * 0.04,
  }),
  loginPasswordTitle: style.text({

  }),
  containerAccess: style.view({
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: windowWidth * 0.05,
  }),
  accessCode: style.text({
    width: windowWidth * 0.39,
    height: windowWidth * 0.05,
    marginRight: windowWidth * 0.02,
    marginLeft: windowWidth * 0.03,
    fontSize: windowWidth * 0.04,
    fontFamily: Fonts.geometria,
  }),
  forgotPassword: style.text({
    fontSize: windowWidth * 0.04,
    marginRight: windowWidth * 0.04,
    color: Color.forgotPassword,
    fontFamily: Fonts.geometria,
  }),
  questionMark:style.image({
    width: windowWidth * 0.05,
    height: windowWidth * 0.05,
    marginTop: windowWidth * 0.005,
    marginRight: windowWidth * 0.17,
  }),
  dottedLineContainer:style.view({
    alignSelf: 'flex-end',
  }),
  dottedLine: style.image({
    marginTop: windowWidth * 0.014,
    marginRight: windowWidth * 0.04,
    width: windowWidth * 0.3,
    height: windowWidth * 0.003,
  }),
  containerButtons: style.view({
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: windowWidth * 0.06,
  }),
  registrationButton: style.view({
    marginRight: windowWidth * 0.05,
    marginLeft: windowWidth * 0.04,
    width: windowWidth * 0.45,
    height: windowWidth * 0.13,
    backgroundColor: Color.forgotPassword,
    borderRadius: windowWidth * 0.067,
  }),
  enterButton: style.view({
    marginRight: windowWidth * 0.05,
    width: windowWidth * 0.45,
    height: windowWidth * 0.13,
    backgroundColor: Color.enterButton,
    borderRadius: windowWidth * 0.067,
  }),
  textReg: style.text({
    marginTop: windowWidth * 0.04,
    marginLeft: windowWidth * 0.1,
    marginRight: windowWidth * 0.1,
    color: Color.white,
    width: windowWidth * 0.26,
    height: windowWidth * 0.05,
    fontSize: windowWidth * 0.039,
    fontFamily: Fonts.geometriaBold,
  }),
  textEnter: style.text({
    marginTop: windowWidth * 0.04,
    marginLeft: windowWidth * 0.17,
    marginRight: windowWidth * 0.17,
    color: Color.white,
    width: windowWidth * 0.13,
    height: windowWidth * 0.07,
    fontSize: windowWidth * 0.039,
    fontFamily: Fonts.geometriaBold,
  }),
  styleSwitchContainer: style.view({
    marginTop: windowWidth * 0.09,
    marginLeft: windowWidth * 0.03,
    marginRight: windowWidth * 0.039,
  }),
  politTextContainer: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
  }),
  politText: style.text({
    marginTop: windowWidth * 0.07,
    fontSize: windowWidth * 0.027,
    width: windowWidth * 0.8,
    fontFamily: Fonts.geometria,
  }),
  underLine:style.image({
    justifyContent: 'flex-end',
    marginTop: windowWidth * 0.001,
    marginLeft: windowWidth * 0.3,
    width: windowWidth * 0.445,
    height: windowWidth * 0.002,
  }),
  modalContent: style.view({
    width: windowWidth * 0.87,
    height: windowWidth * 0.53,
    marginTop: windowWidth * 0.59,
    marginBottom: windowWidth * 0.57,
    marginLeft: windowWidth * 0.02,
    marginRight: windowWidth * 0.09,
    backgroundColor: Color.white,
    borderRadius: windowWidth * 0.017,
  }),
  headerModal: style.view({
    flexDirection: 'row',
    justifyContent: 'space-around',
  }),
  questionMarkModal: style.image({
    width: windowWidth * 0.064,
    height: windowWidth * 0.064,
    marginTop: windowWidth * 0.046,
    marginLeft: windowWidth * 0.04,
    marginRight: windowWidth * 0.032,
  }),
  headerTextModal: style.text({
    width: windowWidth * 0.55,
    height: windowWidth * 0.07,
    marginTop: windowWidth * 0.038,
    marginRight: windowWidth * 0.11,
    fontFamily: Fonts.geometriaBold,
    fontSize: windowWidth * 0.058,
  }),
  closeModal: style.image({
    width: windowWidth * 0.05,
    height: windowWidth * 0.05,
    marginTop: windowWidth * 0.05,
    marginRight: windowWidth * 0.04,
  }),
  lineModalView: style.view({
    marginTop: windowWidth * 0.04,
  }),
  lineModal: style.image({
    width: windowWidth * 0.87,
    height: windowWidth * 0.001,
  }),
  textModal: style.text({
    marginTop: windowWidth * 0.054,
    marginLeft: windowWidth * 0.067,
    marginRight: windowWidth * 0.18,
    fontFamily: Fonts.geometria,
    fontSize: windowWidth * 0.04,
  }),
  passwordModal: style.view({
    width: windowWidth * 0.87,
    height: windowWidth * 0.15,
    marginTop: windowWidth * 0.087,
    backgroundColor: Color.backColorPasswordModal,
    borderBottomLeftRadius: windowWidth * 0.017,
    borderBottomRightRadius: windowWidth * 0.017,
  }),
  textPasswordModal: style.text({
    marginTop: windowWidth * 0.05,
    marginLeft: windowWidth * 0.29,
    marginRight: windowWidth * 0.29,
    fontSize: windowWidth * 0.039,
    fontFamily: Fonts.geometria,
    width: windowWidth * 0.3,
    height: windowWidth * 0.042,
  }),
})
